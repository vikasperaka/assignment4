import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application
message = ''


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title TEXT, director TEXT, actor TEXT, date TEXT, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director']
    actor = request.form['actor'].lower()
    date = request.form['release_date']
    rating = request.form['rating']

    message = []
    try:
        rating_d = float(rating)
        rating = '%.2f'%(rating_d)
    except Exception as exp:
        # message.append("Number not entered for rating.")
        message.append('Movie %s could not be inserted - %s'%(title, str(exp)))
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT title FROM movie WHERE title='%s'"%(title))
        if (len(cur.fetchall()) != 0):
            message.append('Movie %s could not be inserted - Movie already exists in database'%(title))
            return render_template('index.html', message=message)
        cur.execute("INSERT INTO movie (year, title, director, actor, date, rating) values (" + year + ", '" + title + "', '" + director + "', '" + actor + "', '" + date + "', " + rating + ")")
        cnx.commit()
    except Exception as exp:
        message.append('Movie %s could not be inserted - %s'%(title, str(exp)))
        return render_template('index.html', message=message)
    message.append('Movie %s successfully inserted'%(title))
    return render_template('index.html', message=message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director']
    actor = request.form['actor'].lower()
    date = request.form['release_date']
    rating = request.form['rating']

    message = []
    try:
        rating_d = float(rating)
        rating = '%.2f'%(rating_d)
    except:
        message.append("Number not entered for rating.")
        return render_template('index.html', message=message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT title FROM movie WHERE title='%s'"%(title))
        if (len(cur.fetchall()) == 0):
            message.append('Movie with %s does not exist'%(title))
            return render_template('index.html', message=message)
        cur.execute("UPDATE movie SET year = " + year + ", director = '" + director + "', actor = '" + actor + "', date = '" + date + "', rating = " + rating + " WHERE title='" + title + "'")
        cnx.commit()
    except Exception as exp:
        message.append('Movie %s could not be inserted - %s'%(title, str(exp)))
        return render_template('index.html', message=message)
    message.append('Movie %s successfully updated'%(title))
    return render_template('index.html', message=message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['delete_title'].lower()
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    message = []
    try:
        cur.execute("SELECT title FROM movie WHERE title='%s'"%(title))
        if (len(cur.fetchall()) == 0):
            message.append('Movie with %s does not exist'%(title))
            return render_template('index.html', message=message)
        cur.execute("DELETE FROM movie WHERE title='%s'"%(title))
        cnx.commit()
    except Exception as exp:
        message.append('Movie %s could not be deleted - %s'%(title, str(exp)))
        return render_template('index.html', message=message)
    message.append('Movie %s successfully deleted'%(title))
    return render_template('index.html', message=message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    actor = request.args.get('search_actor').lower()
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title,year,actor FROM movie WHERE actor='%s'"%(actor))
    results = cur.fetchall()
    if (len(results) == 0):
        message = 'No movie found for actor %s'%(actor)
    else:
        message = ''
        for row in results:
            message = message + ', '.join(map(str,row)) + "\n";
    message = message.split("\n")
    return render_template('index.html', message=message)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT MAX(rating) FROM movie")
    rating = cur.fetchone()[0]
    cur.execute("SELECT title,year,actor,director,rating FROM movie WHERE rating='%s'"%(rating))
    results = cur.fetchall()
    if (len(results) == 0):
        message = 'No movies in database'
    else:
        message = ''
        for row in results:
            message = message + ', '.join(map(str,row)) + "\n";
    message = message.split("\n")
    return render_template('index.html', message=message)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT MIN(rating) FROM movie")
    rating = cur.fetchone()[0]
    cur.execute("SELECT title,year,actor,director,rating FROM movie WHERE rating='%s'"%(rating))
    results = cur.fetchall()
    if (len(results) == 0):
        message = 'No movies in database'
    else:
        message = ''
        for row in results:
            message = message + ', '.join(map(str,row)) + "\n";
    message = message.split("\n")
    return render_template('index.html', message=message)

create_table()

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
